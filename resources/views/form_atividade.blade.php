@extends('layouts.main')

<!-- CSS ESPECÍFICO DA PÁGINA -->
@section('css_file_extra')
@stop

<!-- JS ESPECÍFICO DA PÁGINA -->
@section('js_files_extra')
<!-- PAGE RELATED PLUGIN(S) -->
<script src="/js/datepicker/daterangepicker.js"></script>
@stop

<!-- JS ESPECÍFICO DA PAGINA -->
@section('scripts')
<script>
    // TRATAMENTO DO STATUS CONCLUÍDO VINDO DO FORMULÁRIO
    $(function(){
        var status_concluido = $('#status_id').val();
        
        if (status_concluido == 4) {
            $('#status_id').attr('disabled', true);
        }
    });
    
    // DATA INÍCIO E FIM DA ATIVIDADE
    $('#data_inicio').datepicker({
        dateFormat: 'dd/mm/yy',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        onSelect: function (selectedDate) {
            $('#data_fim').datepicker('option', 'minDate', selectedDate);
        }
    });

    $('#data_fim').datepicker({
        dateFormat: 'dd/mm/yy',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        onSelect: function (selectedDate) {
            $('#data_inicio').datepicker('option', 'maxDate', selectedDate);
        }
    });

    // TRAMAMENTO DE STATUS CONCLUÍDO
    // REGRAS DE PERMISSÕES
    $(function () {
        $("#status_id").on("change", function (evt) {
            var status = $(this).val();
            var situacao = $('#situacao').val();
            
            if (status == 4) {
                $('#nome').attr('readonly', true);
                $('input[type="text"], textarea').attr('readonly', 'readonly');
                $('#situacao').attr('disabled', true);
                $('#data_inicio').attr('readonly', true).datepicker('destroy');
                $('#data_fim').attr('readonly', true).datepicker('destroy');
                
                $('#situacao').append('<div id="situacao_hidden"><input type="hidden" name="situacao" id="situacao" value="'+situacao+'" /></div>');
            } else {
                $('#nome').removeAttr('readonly');
                $('input[type="text"], textarea').attr('readonly', false);
                $('#situacao').removeAttr('disabled');
                $('#data_inicio').attr('readonly', false).datepicker();
                $('#data_fim').attr('readonly', false).datepicker();
                
                $('#situacao_hidden').last().remove();
            }
        });

        // CHAMADA PARA MUDANÇÃO DE SELEÇÃO
        $('#status_id').trigger('change');
    });
</script>
@stop

@section('content')

<!-- RIBBON PAGE -->
<div id="ribbon">

<!--    <span class="ribbon-button-alignment"> 
        <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
        </span> 
    </span>-->

    <!-- breadcrumb -->
    <ol class="breadcrumb">
        <li>Atividades</li><li>Cadastro</li>
    </ol>
    <!-- end breadcrumb -->

    <!-- You can also add more buttons to the
    ribbon for further usability

    Example below:

    <span class="ribbon-button-alignment pull-right">
    <span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
    <span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
    <span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
    </span> -->

</div>
<!-- END RIBBON PAGE -->

<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-pencil-square-o"></i> Atividades <span>> Cadastro</span></h1>
    </div>
</div>

<div class="row">
    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-3" data-widget-editbutton="false" data-widget-custombutton="false">
            <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                        
                    data-widget-colorbutton="false"	
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true" 
                    data-widget-sortable="false"
                                        
            -->
            <header>
                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                <h2>Registro de atividades</h2>				
            </header>

            <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body no-padding">

                    <form id="order-form" class="smart-form" novalidate="novalidate" action="{{ route('atividadeGravar') }}" method="post">
                        {{ csrf_field() }}
                        @if($atividade->id)
                        <input type="hidden" name="id" value="{{ $atividade->id }}" />
                        @endif

                        <header>
                            Dados para a atividade
                        </header>

                        <fieldset>
                            <div class="row">
                                <section class="col col-6">
                                    <label class="label">Nome da atividade</label>
                                    <label class="input {{ ($errors->has('nome')?"state-error":"" ) }}"> <i class="icon-append fa fa-pencil-square-o"></i>
                                        <input type="text" value="{{ old('nome', $atividade->nome) }}" name="nome" id="nome">
                                    </label>
                                    @if($errors->has('nome'))
                                    <em id="nome-error" class="invalid">
                                        @foreach($errors->get('nome') as $msg)
                                        {{ $msg }}
                                        @endforeach
                                    </em>
                                    @endif
                                </section>
                                <section class="col col-3">
                                    <label class="label">Data Início</label>
                                    <label class="input {{ ($errors->has('data_inicio')?"data_inicio-error":"" ) }}"> <i class="icon-append fa fa-calendar"></i>
                                        @if($atividade->id)
                                            <input type="text" value="{{ old('data_inicio', date('d/m/Y', strtotime($atividade->data_inicio))) }}" name="data_inicio" id="data_inicio">
                                        @else
                                            <input type="text" value="" name="data_inicio" id="data_inicio">
                                        @endif
                                    </label>
                                    @if($errors->has('data_inicio'))
                                    <em id="data_inicio-error" class="invalid">
                                        @foreach($errors->get('data_inicio') as $msg)
                                        {{ $msg }}
                                        @endforeach
                                    </em>
                                    @endif
                                </section>
                                <section class="col col-3">
                                    <label class="label">Data Término</label>
                                    <label class="input {{ ($errors->has('data_fim')?"data_fim-error":"" ) }}"> <i class="icon-append fa fa-calendar"></i>
                                        @if($atividade->id)
                                        <input type="text" value="{{ old('data_fim', date('d/m/Y', strtotime($atividade->data_fim))) }}" name="data_fim" id="data_fim">
                                        @else
                                        <input type="text" value="" name="data_fim" id="data_fim">
                                        @endif
                                    </label>
                                    @if($errors->has('data_fim'))
                                    <em id="data_fim-error" class="invalid">
                                        @foreach($errors->get('data_fim') as $msg)
                                        {{ $msg }}
                                        @endforeach
                                    </em>
                                    @endif
                                </section>
                            </div>

                            <div class="row">
                                <section class="col col-6">
                                    <label class="label">Descrição</label>
                                    <label class="textarea {{ ($errors->has('descricao')?"descricao-error":"" ) }}"> <i class="icon-append fa fa-comment"></i> 										
                                        <textarea rows="5" name="descricao" id="descricao">{{ old('descricao', $atividade->descricao) }}</textarea> 
                                    </label>
                                    @if($errors->has('descricao'))
                                    <em id="descricao-error" class="invalid">
                                        @foreach($errors->get('descricao') as $msg)
                                        {{ $msg }}
                                        @endforeach
                                    </em>
                                    @endif
                                </section>
                                <section class="col col-3">
                                    <label class="label">Status</label>
                                    <label class="select {{ ($errors->has('status_id')?"status_id-error":"" ) }}">
                                        {{ formStatusSelect($atividade->status_id) }}
                                        <i></i>
                                    </label>
                                    @if($errors->has('status_id'))
                                    <em id="status_id-error" class="invalid">
                                        @foreach($errors->get('status_id') as $msg)
                                        {{ $msg }}
                                        @endforeach
                                    </em>
                                    @endif
                                </section>
                                <section class="col col-3">
                                    <label class="label">Situação</label>
                                    <label class="select {{ ($errors->has('situacao')?"situacao-error":"" ) }}">
                                        {{ formSituacaoSelect($atividade->situacao) }}
                                        <i></i>
                                    </label>
                                    @if($errors->has('situacao'))
                                    <em id="situacao-error" class="invalid">
                                        @foreach($errors->get('situacao') as $msg)
                                        {{ $msg }}
                                        @endforeach
                                    </em>
                                    @endif
                                </section>
                            </div>
                        </fieldset>

                        <footer>
                            <button type="submit" class="btn btn-primary">
                                Registar atividade
                            </button>
                        </footer>
                    </form>

                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
    </article>
</div>
@stop
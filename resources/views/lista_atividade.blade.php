@extends('layouts.main')

<!-- CSS ESPECÍFICO DA PÁGINA -->
@section('css_file_extra')
@stop

<!-- JS ESPECÍFICO DA PÁGINA -->
@section('js_files_extra')
<!-- PAGE RELATED PLUGIN(S) -->
<script src="/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script src="/js/plugin/moment/moment.min.js"></script>
@stop

<!-- SCRIPTS ESPECÍFICO DA PAGINA -->
@section('scripts')
<script type="text/javascript">

    // DO NOT REMOVE : GLOBAL FUNCTIONS!

    $(document).ready(function () {

        pageSetUp();

        /* // DOM Position key index //
         
         l - Length changing (dropdown)
         f - Filtering input (search)
         t - The Table! (datatable)
         i - Information (records)
         p - Pagination (paging)
         r - pRocessing 
         < and > - div elements
         <"#id" and > - div with an id
         <"class" and > - div with a class
         <"#id.class" and > - div with an id and class
         
         Also see: http://legacy.datatables.net/usage/features
         */

        /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;
        var responsiveHelper_datatable_fixed_column = undefined;
        var responsiveHelper_datatable_col_reorder = undefined;
        var responsiveHelper_datatable_tabletools = undefined;

        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        var oTable = $('#dt_basic').DataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "processing": true,
            "serverSide": true,
            "ajax": "{{ route('atividadeListaAjax') }}",
            /*            "oLanguage": {
             "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
             "sProcessing": "Processando..."
             },*/
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
            },
//            initComplete: function () {
//                this.api().columns().every(function () {
//                    var column = this;
//                    var select = $('<select><option value=""></option></select>')
//                            .appendTo($(column.footer()).empty())
//                            .on('change', function () {
//                                var val = $.fn.dataTable.util.escapeRegex(
//                                        $(this).val()
//                                        );
//
//                                column
//                                        .search(val ? '^' + val + '$' : '', true, false)
//                                        .draw();
//                            });
//
//                    column.data().unique().sort().each(function (d, j) {
//                        select.append('<option value="' + d + '">' + d + '</option>')
//                    });
//                });
//            },
            "columns": [
                {"data": "id", "orderable": false},
                {"data": "nome", "orderable": false},
                {"data": "descricao", "orderable": false},
                {"data": "data_inicio", "className": "text-center", "orderable": false,
                    'render': function (data) {
                        return moment(data).format('DD/MM/YYYY');
                    }
                },
                {"data": "data_fim", "className": "text-center", "orderable": false,
                    'render': function (data) {
                        return moment(data).format('DD/MM/YYYY');
                    }
                },
                {"data": "status_id", "className": "text-center", "orderable": false,
                    'render': function (data) {
                        if (data == 1)
                            return 'Pendente';
                        else if (data == 2)
                            return 'Em desenvolvimento';
                        else if (data == 3)
                            return 'Em teste';
                        else if (data == 4)
                            return 'Concluído';
                    }
                },
                {"data": "situacao", "className": "text-center", "orderable": false,
                    'render': function (data) {
                        if (data == 1)
                            return 'Ativo';
                        else
                            return 'Inativo';
                    }
                },
                {"data": "id", "orderable": false, "className": "text-center",
                    'render': function (data, type, row) {
                        var action = '<a href="#" atividade="' + data + '" class="ativ_editar icon-append fa fa-pencil-square-o" title="Editar"></a>';
                        return action;
                    }
                }
            ],
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
                console.info(nRow);
                if (data.status_id == 4) {
                    $(nRow).addClass('danger');
                }
                
                $(nRow).attr('data-status', data.status_id);
                $(nRow).attr('data-situacao', data.situacao);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            }
        });

        /* END BASIC */

        // custom toolbar
        $("div.toolbar").html('<div class="text-right"><img src="img/logo-o.png" alt="DuoSystem" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');

        // Apply the filter
        $("#datatable_fixed_column thead th input[type=text]").on('keyup change', function () {

            otable
                    .column($(this).parent().index() + ':visible')
                    .search(this.value)
                    .draw();

        });
        /* END COLUMN FILTER */

        /* COLUMN SHOW - HIDE */
        $('#datatable_col_reorder').DataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "autoWidth": true,
            "oLanguage": {
                "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
            },
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_datatable_col_reorder) {
                    responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_datatable_col_reorder.respond();
            }
        });

        /* END COLUMN SHOW - HIDE */


        /* ATIVIDADE EDITAR */
        $("#dt_basic").on("click", ".ativ_editar", function (e) {
            $ativ = $(this).attr('atividade');
            location = '/atividade/cadastro/' + $ativ;
            e.preventDefault();
        });
        
        /* SELEÇAO DE STATUS */
        $("#dt_basic").on("change", "#status_id", function (e) {
            var status_id = $(e.currentTarget).val();
            
            if (status_id == 0) {
                $('#dt_basic tbody tr').show();
            } else {
                $('#dt_basic tbody tr').show();
                $('#dt_basic tbody tr').each(function(index, element) {
                    if ($(this).data('status') != status_id) {
                        $(this).hide();
                    }
                });
                
            } 
        });
        
        /* SELEÇAO DE SITUAÇÃO */
        $("#dt_basic").on("change", "#situacao", function (e) {
            var situacao_id = $(e.currentTarget).val();
            
            if (situacao_id == 0) {
                $('#dt_basic tbody tr').show();
            } else {
                $('#dt_basic tbody tr').show();
                $('#dt_basic tbody tr').each(function(index, element) {
                    if ($(this).data('situacao') != situacao_id) {
                        $(this).hide();
                    }
                });
                
            } 
        });
    });

</script>
@stop

@section('content')

<!-- RIBBON PAGE -->
<div id="ribbon">

<!--    <span class="ribbon-button-alignment"> 
        <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
        </span> 
    </span>-->

    <!-- breadcrumb -->
    <ol class="breadcrumb">
        <li>Atividades</li><li>Listagem</li>
    </ol>
    <!-- end breadcrumb -->

    <!-- You can also add more buttons to the
    ribbon for further usability

    Example below:

    <span class="ribbon-button-alignment pull-right">
    <span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
    <span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
    <span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
    </span> -->

</div>
<!-- END RIBBON PAGE -->

<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-pencil-square-o"></i> Atividades <span>> Listagem</span></h1>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                    <h2>Listagem de atividades </h2>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a class="btn btn-primary" href="{{ route('atividadeCadastro') }}"><i class="fa fa-pencil-square-o"></i> Nova atividade</a>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">

                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <th>ID</th>
                                <th>Nome</th>
                                <th>Descrição</th>
                                <th>Data início</th>
                                <th>Data fim</th>
                                <th> {{ formStatusSelect() }} </th>
                                <th> {{ formSituacaoSelect() }} </th>
                                <th>Ação</th>
                            </thead>
                        </table>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->

</div>
@stop
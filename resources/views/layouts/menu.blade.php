<ul>
    <li>
        <a href="#"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i> <span class="menu-item-parent">Atividades</span></a>
        <ul>
            <li>
                <a href="{{ route('atividadeCadastro') }}">Cadastro</a>
            </li>
            <li>
                <a href="{{ route('atividadeLista') }}">Listagem</a>
            </li>
        </ul>
    </li>	
</ul>
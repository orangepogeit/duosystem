<div class="project-context hidden-xs">

	<span class="label">Eventos:</span>
	<span class="project-selector dropdown-toggle" data-toggle="dropdown">Eventos recentes <i class="fa fa-angle-down"></i></span>

	<ul class="dropdown-menu">
		<li>
			<a href="javascript:void(0);">A sua UFMG - Calourada DCE 2017/2 - Publicado</a>
		</li>
		<li>
			<a href="javascript:void(0);">Neurosis em São Paulo - Aguardando</a>
		</li>
		<li>
			<a href="javascript:void(0);">1º Seminário da Associação Pró-Ecovilas - Publicado</a>
		</li>
		<li class="divider"></li>
		<li>
			<a href="javascript:void(0);"><i class="fa fa-power-off"></i> Limpar</a>
		</li>
	</ul>

</div>
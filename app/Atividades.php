<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atividades extends Model
{
    // configurações do eloquent
    protected $guarded = ['id'];
    protected $table = 'atividades';
    
    /**
     * Define nomes amigáveis dos campos para ser usado na validação
     * @var array
     */
    public static $fieldNames = [
        'nome' => 'nome da atividade',
        'descricao' => 'descrição',
        'data_inicio' => 'data início',
        'data_fim' => 'data término',
        'status_id' => 'status',
        'situacao' => 'situação'
    ];
    
    /**
     * Define as regras base de validações dos campos, seja na inserção ou no update
     * @var array
     */
    public static $rules = [
        'create' => [
            'nome' => 'required|max:255',
            'descricao' => 'required|max:600',
            'data_inicio' => 'required',
            'status_id' => 'required|numeric',
            'situacao' => 'required|numeric'
        ],
        'update' => [
            'nome' => 'required|max:255',
            'descricao' => 'required|max:600',
            'data_inicio' => 'required',
            'status_id' => 'required|numeric',
            'situacao' => 'required|numeric'
        ]
    ];
}

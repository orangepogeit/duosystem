<?php

/**
 * Retorna o select (dropdown) com os status da atividade
 * @param int $selected ID do status para ser selecionado por padrão
 * @return string html do select
 */
function formStatusSelect($selected = NULL) {
    $types = [
        '' => 'Selecione o status',
        '1' => 'Pendente', 
        '2' => 'Em desenvolvimento',
        '3' => 'Teste',
        '4' => 'Concluído'
    ];
    return Form::select('status_id', $types, $selected, ['id' => 'status_id']);
}

/**
 * Retorna o select (dropdown) com a situaçao da atividade
 * @param type $selected ID da situação para ser selecionado por padrão
 * @return string html do select
 */
function formSituacaoSelect($selected = NULL) {
    $types = [
        '' => 'Selecione a situação',
        '1' => 'Ativo',
        '2' => 'Inativo'
    ];
    return Form::select('situacao', $types, $selected, ['id' => 'situacao']);
}
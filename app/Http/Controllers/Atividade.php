<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Atividades as ModelAtividades;
use App\Status as ModelStatus;

class Atividade extends Controller
{
    /**
     * Método para chamar a view de lista de atividade.
     * 
     * @return type
     */
    public function atividadeLista() {
        return view('lista_atividade');
    }
    
    /**
     * Método para listar as atividades da model
     * @return type
     */
    public function ajaxAtividadeLista() {
        return datatables()->of(ModelAtividades::all())->toJson();
    }
    
    /**
     * Método para enviar dados para o formulário
     * 
     * @param integer ID Atividade
     * @return array Data_Result
     */
    public function showForm($id = 0) {
        if ($id !== 0) {
            $ModelAtividades = ModelAtividades::find($id);
            $this->view_data['atividade'] = $ModelAtividades;
        } else {
            $this->view_data['atividade'] = new ModelAtividades;
        }

        return view('form_atividade', $this->view_data);
    }
    
    /**
     * Método para salvar e atualizar
     * 
     * @param Request $request
     * @return type
     */
    public function persist(Request $request) {
        
        $id = $request->input('id', NULL);
        
        $status = $request->status_id;
        
        $rules = Atividade::rules($status, $id);
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames(ModelAtividades::$fieldNames);

        if ($validator->fails()) {
            return redirect('/atividade/cadastro' . ( $id ? "/$id" : "" ))
                ->withErrors($validator)
                ->withInput();
        }
        $data_inicio = str_replace('/', '-', $request->data_inicio);
        $data_fim = str_replace('/', '-', $request->data_fim);
        
        $data = [
            'nome' => $request->nome, 
            'descricao' => $request->descricao, 
            'data_inicio' => date('Y-m-d', strtotime($data_inicio.''.date('H:i:s'))),
            'data_fim' => date('Y-m-d', strtotime($data_fim.''.date('H:i:s'))),
            'status_id' => $request->status_id,
            'situacao' => $request->situacao
            ];
        
        if ($id) {
            $o = ModelAtividades::find($id);
            $o->update($data);
        } else {
            $o = ModelAtividades::create($data);
        }

        return redirect('/atividade');
    }
    
    /**
     * Retorna regras de validação
     * 
     * @param int $id ID da atividade
     * @return array Retorna array com regras de validação da atividade
     */
    private static function rules($status, $id = null) {
        $base_rules = ( $id ? ModelAtividades::$rules['update'] : ModelAtividades::$rules['create'] );
        
        // Regra do campo DATA FIM
        if ($status == 4) {
            $base_rules['data_fim'] = 'required';
        }
        
        return $base_rules;
    }
    
}